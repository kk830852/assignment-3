import numpy as np
import matplotlib.pyplot as plt
from differentiate_midp import gradient_2point
from physProps import physProps, geoWind, pressure, uGeoExact

def geostrophicWind(n):
    """Calculate the grostrophic with analytically and numerically and plot"""
     
    # Resolution and size of the domain
    
    N = n # the number of intervals to divide space into
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    dy = (ymax - ymin)/N #the legnth of the spacing
    
    # The spatial dimension, y:
    y = np.linspace(ymin, ymax, N+1)
    
    # The pressure at the y points and the exact geostrophic wind
    p = pressure(y, physProps)
    uExact = uGeoExact(y, physProps)
    
    # The pressure gradient and wind using two point differences
    dpdy = gradient_2point(p, dy)
    u_2point = geoWind(dpdy, physProps)
    error = abs(u_2point - uExact)
    
    return u_2point, uExact, error,y
    
    # Graph to compare the numerical and analytical solutions
    # Plot using large font
    font = {"size" : 14}
    plt.rc("font", **font)
    
    # Plot the approximate and exact wind at y points
    plt.plot(y/1000, uExact, "k-", label="Exact")
    plt.plot(y/1000, u_2point, "*k--", label="Two-point differences",\
             ms = 12, markeredgewidth = 1.5, markerfacecolor = "none")
    plt.legend(loc = "best")
    plt.xlabel("y (km)")
    plt.ylabel("u (m/s)")
    plt.savefig("geoWindCent.jpg")
    plt.show()
    
    
    
    
    plt.plot(y/1000, error, "b-", label="Error")
    plt.legend(loc = "best")
    plt.xlabel("y (km)")
    plt.ylabel("u (m/s)")
    plt.savefig("Error.jpg")
    plt.show()
    
if __name__ == "__main__":
    u_2point = geostrophicWind(10)[0]
    uExact = geostrophicWind(10)[1]
    error = geostrophicWind(10)[2]
    y = geostrophicWind(10)[3]
    
    # Graph to compare the numerical and analytical solutions
    # Plot using large font
    font = {"size" : 14}
    plt.rc("font", **font)
    
    # Plot the approximate and exact wind at y points
    plt.plot(y/1000, uExact, "k-", label="Exact")
    plt.plot(y/1000, u_2point, "*k--", label="Two-point differences",\
             ms = 12, markeredgewidth = 1.5, markerfacecolor = "none")
    plt.legend(loc = "best")
    plt.xlabel("y (km)")
    plt.ylabel("u (m/s)")
    plt.savefig("geoWindCent.jpg")
    plt.show()
    
    
    plt.plot(y/1000, error, "b-", label="Error")
    plt.legend(loc = "best")
    plt.xlabel("y (km)")
    plt.ylabel("u (m/s)")
    plt.savefig("Error.jpg")
    plt.show()
    
    
error_array = np.zeros(100)
for i in range(1,100):
    error = geostrophicWind(i*1)[2]
    error = error.tolist()[:-2]
    error = np.asarray(error)
    error_array[i] = np.mean(error)

    #remove outer points in array
    
print(error_array)   
no_points = np.linspace(1,100,100) 
plt.plot(no_points, error_array)
plt.xlabel("Number of points")
plt.ylabel("Average Error")
plt.savefig("Average_Error.jpg")
plt.show()