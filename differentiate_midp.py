import numpy as np


def gradient_2point(f, dx):
    """The gradient of one dimensional array f assuming points are a distance
    dx apart using 2-point differences. Returns array same size as f"""
    
    #Code here is not complete

    #Initialised the array for the gradient to be the same size as f
    
    dfdx = np.zeros_like(f)
    #two point differences at the end points


    # Centered differences for the mid-points
    for i in range(1,len(f)-2):
        dfdx[i] = (f[i-1] - 27*f[i] + 27*f[i+1] - f[i+2])/(24*dx)

    return dfdx
